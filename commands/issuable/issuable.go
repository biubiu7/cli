package issuable

type IssueType string

const (
	TypeIssue    IssueType = "issue"
	TypeIncident IssueType = "incident"
)
